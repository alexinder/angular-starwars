import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../services/people.service';
import { People } from '../model/people';

@Component({
  selector: 'app-people-table',
  templateUrl: './people-table.component.html',
  styleUrls: ['./people-table.component.css']
})
export class PeopleTableComponent implements OnInit {

 person: People[];

  constructor(private peopleService: PeopleService) { }

  ngOnInit() {
    this.peopleService.getPerson().subscribe(
      data => {
        console.log('component received stocks from backend');
        console.log(data);
        this.person = data;
      },
      error => {
        console.log('error occured woops');
        console.log(error);
      }

    )
}

}
