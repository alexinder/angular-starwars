import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { People } from '../model/people';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {
  constructor(private httpClient: HttpClient) { }

  public getPerson(): Observable<People[]> {
    return this.httpClient.get<People[]>('https://swapi.co/api/people/');
    }

}
