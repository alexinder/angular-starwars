export class People {

    constructor(
        public name: string,
        public height: number,
        public mass: number,
        public hairColour: string,
        public skinColour: string,
        public eyeColour: string,
        public birthYear: string,
        public gender: string,
        public homeworld: string,
        public films: string[],
        public species: string[],
        public vehicles: string[],
        public starships: string[]
    ) {}
}